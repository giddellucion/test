public class Almond implements Nuts, Cookable {
	String color;
	String shape;
	// test 123

    // test 234
    @Override
    public void setColor(String color) {
 
        this.color = color;
    }
	@Override
    public void setShape(String shape) {
 
        this.shape = shape;
    }
	@Override
    public void cook(){
        System.out.println("Start cooking.....");
    }
	@Override
    public String toString(){
        return "Almond \n Color: "+color+" \n Shape: "+shape;
    }
}	