public class CandyDemo
{
	public static void main(String[] args)
	{
	Type lollipops = new Type();
	lollipops.setType("Lollipop");
	
	Type jellyBeans = new Type();
	jellyBeans.setType("Jelly Beans");

	System.out.println("CANDY");
	System.out.println();
	displayCandyInformation(lollipops);
	lollipops.isSweet("Taste:	Sweet");
	System.out.println("--------");
	
	displayCandyInformation(jellyBeans);
	System.out.println("--------");
	}
	
	public static void displayCandyInformation(Type type)
    {  
        System.out.println("Type : "+ type.getType());
		
	}
		
}